﻿Public Class BeforeSolve
    Dim sonido_boton As System.Media.SoundPlayer
    Dim sonido_cambiar As System.Media.SoundPlayer
    Dim sonido_error As System.Media.SoundPlayer
    Dim filas, columnas As Integer
    Dim cadena As String

    Private Sub BeforeSolve_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        sonido_boton = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\button.wav")
        sonido_cambiar = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\change.wav")
        sonido_error = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\error.wav")
        LGetRuta.Focus()
    End Sub

    Private Sub LGetRuta_Click(sender As Object, e As EventArgs) Handles LGetRuta.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_boton.Play()
        End If
        OpenFileDialogFile.InitialDirectory = My.Application.Info.DirectoryPath
        If OpenFileDialogFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
            TBRuta.Text = IO.Path.GetFullPath(OpenFileDialogFile.FileName)
            RTBLoader.LoadFile(TBRuta.Text, RichTextBoxStreamType.PlainText)
            filas = RTBLoader.Lines(0)
            columnas = RTBLoader.Lines(1)
            cadena = RTBLoader.Lines(2)
        End If
    End Sub

    Private Sub TBRuta_TextChanged(sender As Object, e As EventArgs) Handles TBRuta.TextChanged

    End Sub

    Private Sub LIniciar_Click(sender As Object, e As EventArgs) Handles LIniciar.Click
        If Len(TBRuta.Text) = 0 Then
            MsgBox("Archivo de laberinto no seleccionado.", MsgBoxStyle.Exclamation, "Maze Studio")
        Else
            If Len(cadena) = (filas * columnas) Then
                If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
                    sonido_cambiar.Play()
                End If
                Solve.RTBGetPath.Text = TBRuta.Text
                MsgBox("Archivo de laberinto compatible:" & vbCrLf & TBRuta.Text, MsgBoxStyle.Information, "Maze Studio")
                Solve.Show()
                Me.Close()
            Else
                If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
                    sonido_error.Play()
                End If
                TBRuta.Text = ""
                RTBLoader.Text = ""
                MsgBox("Archivo de laberinto no compatible.", MsgBoxStyle.Exclamation, "Maze Studio")
            End If
        End If

    End Sub

End Class