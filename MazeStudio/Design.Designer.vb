﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Design
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Design))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LGuardar = New System.Windows.Forms.Label()
        Me.RTBGetPath = New System.Windows.Forms.RichTextBox()
        Me.RTBLoader = New System.Windows.Forms.RichTextBox()
        Me.ToolTipInfo = New System.Windows.Forms.ToolTip(Me.components)
        Me.PBCelda_Llenar = New System.Windows.Forms.PictureBox()
        Me.PBCelda_Borrar = New System.Windows.Forms.PictureBox()
        Me.LVaciarTodo = New System.Windows.Forms.Label()
        Me.LLlenarTodo = New System.Windows.Forms.Label()
        Me.LEntrada = New System.Windows.Forms.Label()
        Me.LSalida = New System.Windows.Forms.Label()
        Me.LSonido = New System.Windows.Forms.Label()
        Me.LUsandoColor = New System.Windows.Forms.Label()
        Me.PBLine = New System.Windows.Forms.PictureBox()
        CType(Me.PBCelda_Llenar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBCelda_Borrar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBLine, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label1.Location = New System.Drawing.Point(44, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(219, 49)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Diseñador"
        '
        'LGuardar
        '
        Me.LGuardar.AutoSize = True
        Me.LGuardar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LGuardar.Font = New System.Drawing.Font("Century Gothic", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LGuardar.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LGuardar.Location = New System.Drawing.Point(75, 584)
        Me.LGuardar.Name = "LGuardar"
        Me.LGuardar.Size = New System.Drawing.Size(150, 39)
        Me.LGuardar.TabIndex = 9
        Me.LGuardar.Text = "Guardar"
        Me.ToolTipInfo.SetToolTip(Me.LGuardar, "Guardar laberinto")
        '
        'RTBGetPath
        '
        Me.RTBGetPath.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RTBGetPath.Cursor = System.Windows.Forms.Cursors.Default
        Me.RTBGetPath.DetectUrls = False
        Me.RTBGetPath.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RTBGetPath.ForeColor = System.Drawing.Color.DimGray
        Me.RTBGetPath.Location = New System.Drawing.Point(5, 3)
        Me.RTBGetPath.MaxLength = 300
        Me.RTBGetPath.Multiline = False
        Me.RTBGetPath.Name = "RTBGetPath"
        Me.RTBGetPath.ReadOnly = True
        Me.RTBGetPath.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.RTBGetPath.Size = New System.Drawing.Size(83, 12)
        Me.RTBGetPath.TabIndex = 19
        Me.RTBGetPath.Text = ""
        Me.RTBGetPath.Visible = False
        '
        'RTBLoader
        '
        Me.RTBLoader.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RTBLoader.Cursor = System.Windows.Forms.Cursors.Default
        Me.RTBLoader.DetectUrls = False
        Me.RTBLoader.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RTBLoader.ForeColor = System.Drawing.Color.DimGray
        Me.RTBLoader.Location = New System.Drawing.Point(96, 3)
        Me.RTBLoader.MaxLength = 2000
        Me.RTBLoader.Name = "RTBLoader"
        Me.RTBLoader.ReadOnly = True
        Me.RTBLoader.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.RTBLoader.Size = New System.Drawing.Size(83, 12)
        Me.RTBLoader.TabIndex = 20
        Me.RTBLoader.Text = ""
        Me.RTBLoader.Visible = False
        '
        'ToolTipInfo
        '
        Me.ToolTipInfo.AutoPopDelay = 300000
        Me.ToolTipInfo.ForeColor = System.Drawing.Color.DimGray
        Me.ToolTipInfo.InitialDelay = 1
        Me.ToolTipInfo.ReshowDelay = 1
        '
        'PBCelda_Llenar
        '
        Me.PBCelda_Llenar.BackgroundImage = Global.MazeStudio.My.Resources.Resources.Boton_Llenar
        Me.PBCelda_Llenar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PBCelda_Llenar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PBCelda_Llenar.Location = New System.Drawing.Point(156, 391)
        Me.PBCelda_Llenar.Name = "PBCelda_Llenar"
        Me.PBCelda_Llenar.Size = New System.Drawing.Size(100, 100)
        Me.PBCelda_Llenar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBCelda_Llenar.TabIndex = 11
        Me.PBCelda_Llenar.TabStop = False
        Me.ToolTipInfo.SetToolTip(Me.PBCelda_Llenar, "Llenar")
        '
        'PBCelda_Borrar
        '
        Me.PBCelda_Borrar.BackgroundImage = Global.MazeStudio.My.Resources.Resources.Boton_Borrar
        Me.PBCelda_Borrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PBCelda_Borrar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PBCelda_Borrar.Location = New System.Drawing.Point(38, 391)
        Me.PBCelda_Borrar.Name = "PBCelda_Borrar"
        Me.PBCelda_Borrar.Size = New System.Drawing.Size(100, 100)
        Me.PBCelda_Borrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBCelda_Borrar.TabIndex = 10
        Me.PBCelda_Borrar.TabStop = False
        Me.ToolTipInfo.SetToolTip(Me.PBCelda_Borrar, "Vacíar")
        '
        'LVaciarTodo
        '
        Me.LVaciarTodo.AutoSize = True
        Me.LVaciarTodo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LVaciarTodo.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LVaciarTodo.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LVaciarTodo.Location = New System.Drawing.Point(31, 195)
        Me.LVaciarTodo.Name = "LVaciarTodo"
        Me.LVaciarTodo.Size = New System.Drawing.Size(103, 21)
        Me.LVaciarTodo.TabIndex = 21
        Me.LVaciarTodo.Text = "Vacíar todo"
        '
        'LLlenarTodo
        '
        Me.LLlenarTodo.AutoSize = True
        Me.LLlenarTodo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LLlenarTodo.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LLlenarTodo.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LLlenarTodo.Location = New System.Drawing.Point(165, 195)
        Me.LLlenarTodo.Name = "LLlenarTodo"
        Me.LLlenarTodo.Size = New System.Drawing.Size(98, 21)
        Me.LLlenarTodo.TabIndex = 22
        Me.LLlenarTodo.Text = "Llenar todo"
        '
        'LEntrada
        '
        Me.LEntrada.AutoSize = True
        Me.LEntrada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LEntrada.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LEntrada.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LEntrada.Location = New System.Drawing.Point(81, 258)
        Me.LEntrada.Name = "LEntrada"
        Me.LEntrada.Size = New System.Drawing.Size(125, 21)
        Me.LEntrada.TabIndex = 23
        Me.LEntrada.Text = "Editar entrada"
        Me.LEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LSalida
        '
        Me.LSalida.AutoSize = True
        Me.LSalida.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LSalida.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSalida.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LSalida.Location = New System.Drawing.Point(91, 322)
        Me.LSalida.Name = "LSalida"
        Me.LSalida.Size = New System.Drawing.Size(105, 21)
        Me.LSalida.TabIndex = 24
        Me.LSalida.Text = "Editar salida"
        Me.LSalida.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LSonido
        '
        Me.LSonido.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LSonido.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSonido.ForeColor = System.Drawing.Color.DimGray
        Me.LSonido.Location = New System.Drawing.Point(58, 135)
        Me.LSonido.Name = "LSonido"
        Me.LSonido.Size = New System.Drawing.Size(180, 20)
        Me.LSonido.TabIndex = 25
        Me.LSonido.Text = "Sonido: Desactivado"
        Me.LSonido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LUsandoColor
        '
        Me.LUsandoColor.Cursor = System.Windows.Forms.Cursors.Default
        Me.LUsandoColor.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LUsandoColor.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LUsandoColor.Location = New System.Drawing.Point(58, 499)
        Me.LUsandoColor.Name = "LUsandoColor"
        Me.LUsandoColor.Size = New System.Drawing.Size(180, 20)
        Me.LUsandoColor.TabIndex = 26
        Me.LUsandoColor.Text = "Usando color: Gris"
        Me.LUsandoColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PBLine
        '
        Me.PBLine.BackColor = System.Drawing.Color.DodgerBlue
        Me.PBLine.Location = New System.Drawing.Point(24, 95)
        Me.PBLine.Name = "PBLine"
        Me.PBLine.Size = New System.Drawing.Size(250, 2)
        Me.PBLine.TabIndex = 27
        Me.PBLine.TabStop = False
        '
        'Design
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(954, 671)
        Me.Controls.Add(Me.PBLine)
        Me.Controls.Add(Me.LUsandoColor)
        Me.Controls.Add(Me.LSonido)
        Me.Controls.Add(Me.LSalida)
        Me.Controls.Add(Me.LEntrada)
        Me.Controls.Add(Me.LLlenarTodo)
        Me.Controls.Add(Me.LVaciarTodo)
        Me.Controls.Add(Me.RTBLoader)
        Me.Controls.Add(Me.RTBGetPath)
        Me.Controls.Add(Me.PBCelda_Llenar)
        Me.Controls.Add(Me.PBCelda_Borrar)
        Me.Controls.Add(Me.LGuardar)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(970, 710)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(970, 710)
        Me.Name = "Design"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Maze Studio"
        CType(Me.PBCelda_Llenar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBCelda_Borrar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBLine, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As Label
    Friend WithEvents LGuardar As Label
    Friend WithEvents PBCelda_Borrar As PictureBox
    Friend WithEvents PBCelda_Llenar As PictureBox
    Friend WithEvents RTBGetPath As RichTextBox
    Friend WithEvents RTBLoader As RichTextBox
    Friend WithEvents ToolTipInfo As ToolTip
    Friend WithEvents LVaciarTodo As Label
    Friend WithEvents LLlenarTodo As Label
    Friend WithEvents LEntrada As Label
    Friend WithEvents LSalida As Label
    Friend WithEvents LSonido As Label
    Friend WithEvents LUsandoColor As Label
    Friend WithEvents PBLine As PictureBox
End Class
