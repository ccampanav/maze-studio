﻿Public Class Solve
    Dim sonido_celda As System.Media.SoundPlayer
    Dim sonido_boton As System.Media.SoundPlayer
    Dim sonido_cambiar As System.Media.SoundPlayer
    Dim sonido_error As System.Media.SoundPlayer
    Dim sizeT, filas, columnas, nceldas, celda_tam, pointX, pointY As Integer 'Tamaño total para usar en laberinto, num de filas, num de columnas, num de caltas, tamaño de cada celda , auxiliars para iniciar.
    Dim nEntrada, nSalida As Integer 'Id para entrada y salida
    Dim str(1600) As Byte 'Arreglo almacena laberinto
    Dim matriz(40, 40) As Byte 'Arreglo almacena laberinto en forma de matriz
    Dim cell_now As Integer 'Celda actual 
    Dim JumpsOnWay(1600), contSTr As UShort 'Arreglo con saltos del camino mas corto con su delimitador
    Dim JumpOnWayIm(1600) As String 'Arreglo con imagenes por celda
    Dim array(1600, 4), copia_array(1600, 4) As UShort 'Matriz con conexiones por celda
    Dim st_pausa As Boolean 'Estado en pausa?
    Dim marcador, cont_cell As Short 'Variable de Action
    Dim direction As String 'Variable de Action
    Dim error_found As Boolean 'True= Si el laberinto no tiene solución

    Private Sub Solve_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        st_pausa = False
        sonido_celda = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\cell.wav")
        sonido_boton = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\button.wav")
        sonido_cambiar = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\change.wav")
        sonido_error = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\error.wav")
        sizeT = 640 : nEntrada = 8 : nSalida = 9
        If Not My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            LSonido.Text = "Sonido: Desactivado"
            LSonido.ForeColor = Color.DimGray
        Else
            LSonido.Text = "Sonido: Activado"
            LSonido.ForeColor = Color.DodgerBlue
        End If
        RTBLoader.LoadFile(RTBGetPath.Text, RichTextBoxStreamType.PlainText)
        filas = RTBLoader.Lines(0)
        columnas = RTBLoader.Lines(1)
        nceldas = filas * columnas
        For i = 1 To nceldas Step 1
            str(i) = Microsoft.VisualBasic.Mid(RTBLoader.Lines(2), i, 1)
        Next
        If filas > columnas Then
            celda_tam = Fix(sizeT / filas)
        Else
            celda_tam = Fix(sizeT / columnas)
        End If
        If filas = columnas Then
            celda_tam = Fix(sizeT / filas)
        End If
        If Not celda_tam Mod 2 = 0 Then
            celda_tam -= 1
        End If
        Dim aux1, auxX, auxY As Integer
        pointX = 298 : pointY = 16 : aux1 = 1 : auxX = 1 : auxY = 1
        For i = 1 To nceldas Step 1
            Dim newCell As System.Windows.Forms.PictureBox
            newCell = New Windows.Forms.PictureBox
            newCell.Name = "Celda_" & i
            newCell.Left = pointX
            newCell.Top = pointY
            newCell.Width = celda_tam
            newCell.Height = celda_tam
            newCell.Cursor = Cursors.Default
            newCell.BorderStyle = BorderStyle.None
            newCell.BackgroundImageLayout = ImageLayout.Stretch
            newCell.SizeMode = PictureBoxSizeMode.StretchImage
            Select Case str(i)
                Case 0
                    matriz(auxY, auxX) = 0
                    newCell.BackgroundImage = My.Resources.celda_0
                Case 1
                    matriz(auxY, auxX) = 1
                    newCell.BackgroundImage = My.Resources.celda_1
                Case nEntrada
                    matriz(auxY, auxX) = nEntrada
                    newCell.BackgroundImage = My.Resources.Celda_Entrada
                Case nSalida
                    matriz(auxY, auxX) = nSalida
                    newCell.BackgroundImage = My.Resources.Celda_Salida
            End Select
            newCell.Enabled = True
            newCell.Visible = True
            Me.Controls.Add(newCell)
            aux1 += 1 : pointX += celda_tam : auxX += 1
            If aux1 = columnas + 1 Then
                auxX = 1 : auxY += 1
                aux1 = 1 : pointY += celda_tam : pointX = 298
            End If
        Next
        PBBorderRight.Height = (celda_tam * filas) + 1
        PBBorderLeft.Height = (celda_tam * filas) + 1
        PBBorderUp.Width = (celda_tam * columnas) + 1
        PBBorderDown.Width = (celda_tam * columnas) + 1
        PBBorderRight.Left = 298 + (celda_tam * columnas)
        PBBorderDown.Top = 16 + (celda_tam * filas)
    End Sub

    Private Sub LSonido_Click(sender As Object, e As EventArgs) Handles LSonido.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            My.Computer.FileSystem.DeleteFile("conf\sonido.txt")
            LSonido.Text = "Sonido: Desactivado"
            LSonido.ForeColor = Color.DimGray
            sonido_boton.Play()
        Else
            My.Computer.FileSystem.WriteAllText("conf\sonido.txt", "1", False)
            LSonido.Text = "Sonido: Activado"
            LSonido.ForeColor = Color.DodgerBlue
        End If
    End Sub

    Private Sub LPausar_Click(sender As Object, e As EventArgs) Handles LPausar.Click
        st_pausa = True
        PBSpinner.Visible = False
        LEstado.Text = "Estado: Pausado"
        TimerAction.Enabled = False
        LIniciar.Enabled = True
        LPausar.Enabled = False
        LDetener.Enabled = True
    End Sub

    Private Sub LDetener_Click(sender As Object, e As EventArgs) Handles LDetener.Click
        st_pausa = False
        PBSpinner.Visible = False
        LIniciar.Enabled = True
        LPausar.Enabled = False
        LDetener.Enabled = False
        LCeldaActual.Text = "Celda actual: 0"
        LCeldasVisitadas.Text = "Celdas visitadas: 0"
        LEstado.Text = "Sin iniciar"
        For i = 1 To contSTr Step 1
            For Each cell In Me.Controls
                If cell.name = "Celda_" & JumpsOnWay(i) Then
                    cell.image = Nothing
                End If
            Next
        Next
    End Sub

    Private Sub LIniciar_Click(sender As Object, e As EventArgs) Handles LIniciar.Click
        error_found = False
        If st_pausa = False Then
            Call Solution()
            If error_found = False Then
                Dim n As Integer
                n = FindEntrada()
                cell_now = n
                LCeldaActual.Text = "Celda actual: " & cell_now
                marcador = 1 : direction = "" : cont_cell = 1
            End If
        Else
            st_pausa = False
        End If
        If error_found = False Then
            TimerAction.Enabled = True
            PBSpinner.Visible = True
            LIniciar.Enabled = False
            LPausar.Enabled = True
            LDetener.Enabled = True
            LEstado.Text = "Estado: Ejecutando"
        End If
    End Sub

    Private Sub TimerAction_Tick(sender As Object, e As EventArgs) Handles TimerAction.Tick
        PBAction.Increment(1)
        If PBAction.Value Mod 20 = 0 Then
            If marcador = contSTr + 1 Then
                TimerAction.Enabled = False
                PBSpinner.Visible = False
                LPausar.Enabled = False
                LEstado.Text = "Estado: Finalizado"
            Else
                Call Action()
            End If
        End If
        If PBAction.Value = 100 Then
            PBAction.Value = 1
        End If
    End Sub

    Private Sub Action()
        For Each cell In Me.Controls
            If cell.name = "Celda_" & JumpsOnWay(marcador) Then
                Select Case JumpOnWayIm(marcador)
                    Case "up"
                        cell.image = My.Resources.id_up
                    Case "down"
                        cell.image = My.Resources.id_down
                    Case "right"
                        cell.image = My.Resources.id_right
                    Case "left"
                        cell.image = My.Resources.id_left
                    Case "upright"
                        cell.image = My.Resources.id_up_right
                    Case "upleft"
                        cell.image = My.Resources.id_up_left
                    Case "downright"
                        cell.image = My.Resources.id_down_right
                    Case "downleft"
                        cell.image = My.Resources.id_down_left
                    Case ""
                        cell.image = My.Resources.id
                End Select
            End If
        Next
        LCeldaActual.Text = "Celda actual: " & JumpsOnWay(marcador)
        If Not cont_cell = 0 Then
            LCeldasVisitadas.Text = "Celdas visitadas: " & cont_cell
        End If
        marcador += 1 : cont_cell += 1
    End Sub

    Function FindEntrada()
        Dim n As Integer
        For i = 1 To nceldas Step 1
            If str(i) = nEntrada Then
                n = i
            End If
        Next
        Return (n)
    End Function

    Function FindSalida()
        Dim n As Integer
        For i = 1 To nceldas Step 1
            If str(i) = nSalida Then
                n = i
            End If
        Next
        Return (n)
    End Function

    Private Sub Solution()
        Dim NumEntrada, NumSalida, cont_celdas, aux1 As Integer
        NumEntrada = FindEntrada()
        NumSalida = FindSalida()
        cont_celdas = 0
        For i = 1 To nceldas Step 1 'Llenar array de ceros
            For j = 1 To 4 Step 1
                Array(i, j) = 0
            Next
        Next
        For f = 1 To filas Step 1
            For c = 1 To columnas Step 1
                aux1 = 0 : cont_celdas += 1
                If aux1 = 0 Then
                    If f = 1 And c = 1 Then 'EsqSupIzq
                        If Not matriz(f, c) = 1 Then
                            aux1 = 1
                            '###### Abajo ######
                            If Not matriz(f + 1, c) = 1 Then
                                Array(cont_celdas, 2) = M_Abajo(cont_celdas)
                            Else
                                Array(cont_celdas, 2) = 0
                            End If
                            '###################
                            '##### Derecha #####
                            If Not matriz(f, c + 1) = 1 Then
                                Array(cont_celdas, 3) = M_Derecha(cont_celdas)
                            Else
                                Array(cont_celdas, 3) = 0
                            End If
                            '###################
                        End If
                        'Revisar en: derecha y abajo
                    End If
                End If
                If aux1 = 0 Then
                    If f = 1 Then 'Arriba
                        If c > 1 And c < columnas Then
                            If Not matriz(f, c) = 1 Then
                                aux1 = 1
                                '###### Abajo ######
                                If Not matriz(f + 1, c) = 1 Then
                                    Array(cont_celdas, 2) = M_Abajo(cont_celdas)
                                Else
                                    Array(cont_celdas, 2) = 0
                                End If
                                '###################
                                '##### Derecha #####
                                If Not matriz(f, c + 1) = 1 Then
                                    Array(cont_celdas, 3) = M_Derecha(cont_celdas)
                                Else
                                    Array(cont_celdas, 3) = 0
                                End If
                                '###################
                                '#### Izquierda ####
                                If Not matriz(f, c - 1) = 1 Then
                                    Array(cont_celdas, 4) = M_Izquierda(cont_celdas)
                                Else
                                    Array(cont_celdas, 4) = 0
                                End If
                                '###################
                            End If
                            'Revisar en: Derecha, abajo e izquierda
                        End If
                    End If
                End If
                If aux1 = 0 Then
                    If f = 1 And c = columnas Then 'EsqSupDer
                        If Not matriz(f, c) = 1 Then
                            aux1 = 1
                            '###### Abajo ######
                            If Not matriz(f + 1, c) = 1 Then
                                Array(cont_celdas, 2) = M_Abajo(cont_celdas)
                            Else
                                Array(cont_celdas, 2) = 0
                            End If
                            '###################
                            '#### Izquierda ####
                            If Not matriz(f, c - 1) = 1 Then
                                Array(cont_celdas, 4) = M_Izquierda(cont_celdas)
                            Else
                                Array(cont_celdas, 4) = 0
                            End If
                            '###################
                        End If
                        'Revisar en: Abajo e izquierda
                    End If
                End If
                If aux1 = 0 Then
                    If c = columnas Then 'Derecha
                        If f > 1 And f < filas Then
                            If Not matriz(f, c) = 1 Then
                                aux1 = 1
                                '###### Arriba ######
                                If Not matriz(f - 1, c) = 1 Then
                                    Array(cont_celdas, 1) = M_Arriba(cont_celdas)
                                Else
                                    Array(cont_celdas, 1) = "00"
                                End If
                                '###################
                                '###### Abajo ######
                                If Not matriz(f + 1, c) = 1 Then
                                    Array(cont_celdas, 2) = M_Abajo(cont_celdas)
                                Else
                                    Array(cont_celdas, 2) = 0
                                End If
                                '###################
                                '#### Izquierda ####
                                If Not matriz(f, c - 1) = 1 Then
                                    Array(cont_celdas, 4) = M_Izquierda(cont_celdas)
                                Else
                                    Array(cont_celdas, 4) = 0
                                End If
                                '###################
                            End If
                            'Revisar en: Arriba, abajo e izquierda
                        End If
                    End If
                End If
                If aux1 = 0 Then
                    If f = filas And c = columnas Then 'EsqInfDer
                        If Not matriz(f, c) = 1 Then
                            aux1 = 1
                            '###### Arriba ######
                            If Not matriz(f - 1, c) = 1 Then
                                Array(cont_celdas, 1) = M_Arriba(cont_celdas)
                            Else
                                Array(cont_celdas, 1) = 0
                            End If
                            '###################
                            '#### Izquierda ####
                            If Not matriz(f, c - 1) = 1 Then
                                Array(cont_celdas, 4) = M_Izquierda(cont_celdas)
                            Else
                                Array(cont_celdas, 4) = 0
                            End If
                            '###################
                        End If
                        'Revisar en: Arriba e izquierda
                    End If
                End If
                If aux1 = 0 Then
                    If f = filas Then 'Abajo
                        If c > 1 And c < columnas Then
                            If Not matriz(f, c) = 1 Then
                                aux1 = 1
                                '###### Arriba ######
                                If Not matriz(f - 1, c) = 1 Then
                                    Array(cont_celdas, 1) = M_Arriba(cont_celdas)
                                Else
                                    Array(cont_celdas, 1) = 0
                                End If
                                '###################
                                '##### Derecha #####
                                If Not matriz(f, c + 1) = 1 Then
                                    Array(cont_celdas, 3) = M_Derecha(cont_celdas)
                                Else
                                    Array(cont_celdas, 3) = 0
                                End If
                                '###################
                                '#### Izquierda ####
                                If Not matriz(f, c - 1) = 1 Then
                                    Array(cont_celdas, 4) = M_Izquierda(cont_celdas)
                                Else
                                    Array(cont_celdas, 4) = 0
                                End If
                                '###################
                            End If
                            'Revisar en: Arriba, derecha e izquierda
                        End If
                    End If
                End If
                If aux1 = 0 Then
                    If f = filas And c = 1 Then 'EsqInfIzq
                        If Not matriz(f, c) = 1 Then
                            aux1 = 1
                            '###### Arriba ######
                            If Not matriz(f - 1, c) = 1 Then
                                Array(cont_celdas, 1) = M_Arriba(cont_celdas)
                            Else
                                Array(cont_celdas, 1) = 0
                            End If
                            '###################
                            '##### Derecha #####
                            If Not matriz(f, c + 1) = 1 Then
                                Array(cont_celdas, 3) = M_Derecha(cont_celdas)
                            Else
                                Array(cont_celdas, 3) = 0
                            End If
                            '###################
                        End If
                        'Revisar en: Arriba y derecha
                    End If
                End If
                If aux1 = 0 Then
                    If c = 1 Then 'Izquierda
                        If f > 1 And f < filas Then
                            If Not matriz(f, c) = 1 Then
                                aux1 = 1
                                '###### Arriba ######
                                If Not matriz(f - 1, c) = 1 Then
                                    Array(cont_celdas, 1) = M_Arriba(cont_celdas)
                                Else
                                    Array(cont_celdas, 1) = 0
                                End If
                                '###################
                                '##### Derecha #####
                                If Not matriz(f, c + 1) = 1 Then
                                    Array(cont_celdas, 3) = M_Derecha(cont_celdas)
                                Else
                                    Array(cont_celdas, 3) = 0
                                End If
                                '###################
                                '###### Abajo ######
                                If Not matriz(f + 1, c) = 1 Then
                                    Array(cont_celdas, 2) = M_Abajo(cont_celdas)
                                Else
                                    Array(cont_celdas, 2) = 0
                                End If
                                '###################
                            End If
                            'Revisar en: Arriba, derecha y abajo
                        End If
                    End If
                End If
                If aux1 = 0 Then 'Ninguno de los anteriores
                    If Not matriz(f, c) = 1 Then
                        aux1 = 1
                        '###### Arriba ######
                        If Not matriz(f - 1, c) = 1 Then
                            Array(cont_celdas, 1) = M_Arriba(cont_celdas)
                        Else
                            Array(cont_celdas, 1) = 0
                        End If
                        '###################
                        '###### Abajo ######
                        If Not matriz(f + 1, c) = 1 Then
                            Array(cont_celdas, 2) = M_Abajo(cont_celdas)
                        Else
                            Array(cont_celdas, 2) = 0
                        End If
                        '###################
                        '##### Derecha #####
                        If Not matriz(f, c + 1) = 1 Then
                            Array(cont_celdas, 3) = M_Derecha(cont_celdas)
                        Else
                            Array(cont_celdas, 3) = 0
                        End If
                        '###################
                        '#### Izquierda ####
                        If Not matriz(f, c - 1) = 1 Then
                            Array(cont_celdas, 4) = M_Izquierda(cont_celdas)
                        Else
                            Array(cont_celdas, 4) = 0
                        End If
                        '###################
                    End If
                    'Revisar en: Arriba, derecha, abajo e izquierda
                End If
            Next
        Next
        For i = 1 To 1600 Step 1 'Duplicar valores de array() en copia_array
            For j = 1 To 4 Step 1
                copia_array(i, j) = array(i, j)
            Next
        Next
        '############################## ALGORITMO DE DIJKSTRA ##############################
        Dim aux_table(nceldas, 1) As UShort 'Indices y auxiliares para table
        Dim table(nceldas, 50, 1) As UShort 'Matriz multidimensional para almacenar conexiones. 0=Acumulador, 1=Nodo prodecesor
        Dim n As Integer
        Dim exit_now As Boolean
        Dim acum As Integer
        exit_now = False
        For i = 0 To 1 Step 1
            For j = 1 To nceldas Step 1
                Select Case i
                    Case 0
                        aux_table(j, i) = 1 'Indice de la posiciones donde la conexión será agregada
                    Case 1
                        If Array(j, 1) = 0 And Array(j, 2) = 0 And Array(j, 3) = 0 And Array(j, 4) = 0 Then
                            aux_table(j, i) = 0
                        Else
                            aux_table(j, i) = 1 'Auxiliar para saber si una columna ya no almacenará conexiones. 1=continuar, 0=Omitir
                        End If
                End Select
            Next
        Next
        acum = 0
        table(n, aux_table(n, 0), 0) = acum
        table(n, aux_table(n, 0), 1) = 0
        aux_table(n, 0) += 1
        aux_table(n, 1) = 0
        For i = 1 To nceldas Step 1
            For j = 1 To 4 Step 1
                If Array(i, j) = n Then
                    Array(i, j) = 0
                End If
            Next
        Next
        Dim acum_temp As Integer
        acum_temp = 0 : error_found = False
        While exit_now = False
            If acum = 0 Then 'Sólo hacer la primera vez en el bucle
                n = NumEntrada
                For i = 1 To 4 Step 1 'Llenar nodos conectados
                    If Not array(n, i) = 0 Then
                        table(array(n, i), aux_table(array(n, i), 0), 0) = acum + 1
                        table(array(n, i), aux_table(array(n, i), 0), 1) = n
                        aux_table(array(n, i), 0) += 1
                    End If
                Next
            End If
            For n_celdas = 1 To nceldas Step 1 'Buscar el valor del acumualdor
                For index = 1 To aux_table(n_celdas, 0) - 1 Step 1
                    If aux_table(n_celdas, 1) = 1 Then
                        If table(n_celdas, index, 0) < acum Then
                            acum_temp = table(n_celdas, index, 0)
                            For i = 1 To nceldas Step 1 'Borrar a n de la matriz array
                                For j = 1 To 4 Step 1
                                    If array(i, j) = n_celdas Then
                                        array(i, j) = 0
                                    End If
                                Next
                            Next
                            aux_table(n_celdas, 1) = 0 'Cambiar a 0 su indicador de llenado
                            For i = 1 To 4 Step 1 'Llenar nodos conectados
                                If Not array(n_celdas, i) = 0 Then
                                    table(array(n_celdas, i), aux_table(array(n_celdas, i), 0), 0) = acum_temp + 1
                                    table(array(n_celdas, i), aux_table(array(n_celdas, i), 0), 1) = n_celdas
                                    aux_table(array(n_celdas, i), 0) += 1
                                End If
                            Next
                        Else
                            If table(n_celdas, index, 0) = acum Then
                                acum_temp = table(n_celdas, index, 0)
                                For i = 1 To nceldas Step 1 'Borrar a n de la matriz array
                                    For j = 1 To 4 Step 1
                                        If array(i, j) = n_celdas Then
                                            array(i, j) = 0
                                        End If
                                    Next
                                Next
                                aux_table(n_celdas, 1) = 0 'Cambiar a 0 su indicador de llenado
                                For i = 1 To 4 Step 1 'Llenar nodos conectados
                                    If Not array(n_celdas, i) = 0 Then
                                        table(array(n_celdas, i), aux_table(array(n_celdas, i), 0), 0) = acum_temp + 1
                                        table(array(n_celdas, i), aux_table(array(n_celdas, i), 0), 1) = n_celdas
                                        aux_table(array(n_celdas, i), 0) += 1
                                    End If
                                Next
                            End If
                        End If
                    End If
                Next
            Next
            exit_now = True 'Comprobar que ya no hay nada que agregar a table
            For i = 1 To nceldas Step 1
                If aux_table(i, 1) = 1 Then
                    exit_now = False
                End If
            Next
            acum += 1 'Aumentar acumulador
            If acum >= 100000 Then
                error_found = True : Exit While
            End If
        End While
        If error_found = True Then
            sonido_error.Play()
            MsgBox("El laberinto actual no tiene solución.", MsgBoxStyle.Exclamation, "Maze Studio")
            Call LDetener_Click(Nothing, Nothing)
        Else
            '###################################################################################
            Dim indice, value_indice, subindice As Integer
            Dim strWayMoreShort As String
            strWayMoreShort = NumSalida & ","
            Dim exit_to_found As Boolean
            indice = NumSalida : contSTr = 1
            exit_to_found = False
            While exit_to_found = False
                For i = 1 To aux_table(indice, 0) - 1 Step 1
                    If i = 1 Then
                        subindice = i
                        value_indice = table(indice, i, 0)
                    Else
                        If table(indice, i, 0) < value_indice Then
                            subindice = i
                            value_indice = table(indice, i, 0)
                        End If
                    End If
                Next
                contSTr += 1
                strWayMoreShort = strWayMoreShort & table(indice, subindice, 1) & ","
                indice = table(indice, subindice, 1)
                If indice = NumEntrada Then
                    exit_to_found = True
                End If
            End While
            Dim ini, exp, auxJump, helpJump(contSTr), aux_inv As Integer
            auxJump = 0 : ini = 1 : exp = 1 : aux_inv = contSTr
            For i = 1 To Len(strWayMoreShort) Step 1
                If Microsoft.VisualBasic.Mid(strWayMoreShort, i, 1) = "," Then
                    auxJump += 1
                    helpJump(auxJump) = Microsoft.VisualBasic.Mid(strWayMoreShort, ini, exp)
                    exp = 1
                    ini = i + 1
                Else
                    exp += 1
                End If
            Next
            For i = 1 To contSTr Step 1
                JumpsOnWay(i) = helpJump(aux_inv)
                aux_inv -= 1
            Next
            JumpsOnWay(contSTr + 1) = JumpsOnWay(contSTr)
            For i = 1 To 4 Step 1
                If copia_array(JumpsOnWay(1), i) = JumpsOnWay(2) Then
                    Select Case i
                        Case 1
                            JumpOnWayIm(1) = "up"
                        Case 2
                            JumpOnWayIm(1) = "down"
                        Case 3
                            JumpOnWayIm(1) = "right"
                        Case 4
                            JumpOnWayIm(1) = "left"
                    End Select
                End If
            Next
            Dim n_before, n_after As Integer
            Dim s_before, s_after As String
            For i = 2 To contSTr Step 1
                n_before = i - 1 : n_after = i + 1
                s_before = JumpOnWayIm(n_before)
                For j = 1 To 4 Step 1
                    If copia_array(JumpsOnWay(i), j) = JumpsOnWay(n_after) Then
                        Select Case j
                            Case 1
                                s_after = "up"
                            Case 2
                                s_after = "down"
                            Case 3
                                s_after = "right"
                            Case 4
                                s_after = "left"
                        End Select
                    End If
                Next
                '########## UP ##########
                If s_before = "up" And s_after = "up" Then
                    JumpOnWayIm(i) = "up"
                End If
                If s_before = "up" And s_after = "down" Then
                    JumpOnWayIm(i) = ""
                End If
                If s_before = "up" And s_after = "right" Then
                    JumpOnWayIm(i) = "upright"
                End If
                If s_before = "up" And s_after = "left" Then
                    JumpOnWayIm(i) = "upleft"
                End If
                If s_before = "up" And s_after = "upright" Then
                    JumpOnWayIm(i) = "up"
                End If
                If s_before = "up" And s_after = "upleft" Then
                    JumpOnWayIm(i) = "up"
                End If
                If s_before = "up" And s_after = "downright" Then
                    JumpOnWayIm(i) = "right"
                End If
                If s_before = "up" And s_after = "downleft" Then
                    JumpOnWayIm(i) = "left"
                End If
                '########## DOWN ##########
                If s_before = "down" And s_after = "up" Then
                    JumpOnWayIm(i) = ""
                End If
                If s_before = "down" And s_after = "down" Then
                    JumpOnWayIm(i) = "down"
                End If
                If s_before = "down" And s_after = "right" Then
                    JumpOnWayIm(i) = "downright"
                End If
                If s_before = "down" And s_after = "left" Then
                    JumpOnWayIm(i) = "downleft"
                End If
                If s_before = "down" And s_after = "upright" Then
                    JumpOnWayIm(i) = "right"
                End If
                If s_before = "down" And s_after = "upleft" Then
                    JumpOnWayIm(i) = "left"
                End If
                If s_before = "down" And s_after = "downright" Then
                    JumpOnWayIm(i) = "down"
                End If
                If s_before = "down" And s_after = "downleft" Then
                    JumpOnWayIm(i) = "down"
                End If
                '########## RIGHT ##########
                If s_before = "right" And s_after = "up" Then
                    JumpOnWayIm(i) = "upright"
                End If
                If s_before = "right" And s_after = "down" Then
                    JumpOnWayIm(i) = "downright"
                End If
                If s_before = "right" And s_after = "right" Then
                    JumpOnWayIm(i) = "right"
                End If
                If s_before = "right" And s_after = "left" Then
                    JumpOnWayIm(i) = ""
                End If
                If s_before = "right" And s_after = "upright" Then
                    JumpOnWayIm(i) = "right"
                End If
                If s_before = "right" And s_after = "upleft" Then
                    JumpOnWayIm(i) = "up"
                End If
                If s_before = "right" And s_after = "downright" Then
                    JumpOnWayIm(i) = "right"
                End If
                If s_before = "right" And s_after = "downleft" Then
                    JumpOnWayIm(i) = "down"
                End If
                '########## LEFT ##########
                If s_before = "left" And s_after = "up" Then
                    JumpOnWayIm(i) = "upleft"
                End If
                If s_before = "left" And s_after = "down" Then
                    JumpOnWayIm(i) = "downleft"
                End If
                If s_before = "left" And s_after = "right" Then
                    JumpOnWayIm(i) = ""
                End If
                If s_before = "left" And s_after = "left" Then
                    JumpOnWayIm(i) = "left"
                End If
                If s_before = "left" And s_after = "upright" Then
                    JumpOnWayIm(i) = "up"
                End If
                If s_before = "left" And s_after = "upleft" Then
                    JumpOnWayIm(i) = "left"
                End If
                If s_before = "left" And s_after = "downright" Then
                    JumpOnWayIm(i) = "down"
                End If
                If s_before = "left" And s_after = "downleft" Then
                    JumpOnWayIm(i) = "left"
                End If
                '########## UPRIGHT ##########
                If s_before = "upright" And s_after = "up" Then
                    JumpOnWayIm(i) = "up"
                End If
                If s_before = "upright" And s_after = "down" Then
                    JumpOnWayIm(i) = "downright"
                End If
                If s_before = "upright" And s_after = "right" Then
                    JumpOnWayIm(i) = "right"
                End If
                If s_before = "upright" And s_after = "left" Then
                    JumpOnWayIm(i) = "upleft"
                End If
                If s_before = "upright" And s_after = "upright" Then
                    JumpOnWayIm(i) = "upright"
                End If
                If s_before = "upright" And s_after = "upleft" Then
                    JumpOnWayIm(i) = "up"
                End If
                If s_before = "upright" And s_after = "downright" Then
                    JumpOnWayIm(i) = "right"
                End If
                If s_before = "upright" And s_after = "downleft" Then
                    JumpOnWayIm(i) = "upleft"
                End If
                '########## UPLEFT ##########
                If s_before = "upleft" And s_after = "up" Then
                    JumpOnWayIm(i) = "up"
                End If
                If s_before = "upleft" And s_after = "down" Then
                    JumpOnWayIm(i) = "downleft"
                End If
                If s_before = "upleft" And s_after = "right" Then
                    JumpOnWayIm(i) = "upright"
                End If
                If s_before = "upleft" And s_after = "left" Then
                    JumpOnWayIm(i) = "left"
                End If
                If s_before = "upleft" And s_after = "upright" Then
                    JumpOnWayIm(i) = "up"
                End If
                If s_before = "upleft" And s_after = "upleft" Then
                    JumpOnWayIm(i) = "upleft"
                End If
                If s_before = "upleft" And s_after = "downright" Then
                    JumpOnWayIm(i) = "upright"
                End If
                If s_before = "upleft" And s_after = "downleft" Then
                    JumpOnWayIm(i) = "left"
                End If
                '########## DOWNRIGHT ##########
                If s_before = "downright" And s_after = "up" Then
                    JumpOnWayIm(i) = "upright"
                End If
                If s_before = "downright" And s_after = "down" Then
                    JumpOnWayIm(i) = "down"
                End If
                If s_before = "downright" And s_after = "right" Then
                    JumpOnWayIm(i) = "right"
                End If
                If s_before = "downright" And s_after = "left" Then
                    JumpOnWayIm(i) = "downleft"
                End If
                If s_before = "downright" And s_after = "upright" Then
                    JumpOnWayIm(i) = "right"
                End If
                If s_before = "downright" And s_after = "upleft" Then
                    JumpOnWayIm(i) = "downleft"
                End If
                If s_before = "downright" And s_after = "downright" Then
                    JumpOnWayIm(i) = "downright"
                End If
                If s_before = "downright" And s_after = "downleft" Then
                    JumpOnWayIm(i) = "down"
                End If
                '########## DOWNLEFT ##########
                If s_before = "downleft" And s_after = "up" Then
                    JumpOnWayIm(i) = "upleft"
                End If
                If s_before = "downleft" And s_after = "down" Then
                    JumpOnWayIm(i) = "down"
                End If
                If s_before = "downleft" And s_after = "right" Then
                    JumpOnWayIm(i) = "downright"
                End If
                If s_before = "downleft" And s_after = "left" Then
                    JumpOnWayIm(i) = "left"
                End If
                If s_before = "downleft" And s_after = "upright" Then
                    JumpOnWayIm(i) = "downright"
                End If
                If s_before = "downleft" And s_after = "upleft" Then
                    JumpOnWayIm(i) = "left"
                End If
                If s_before = "downleft" And s_after = "downright" Then
                    JumpOnWayIm(i) = "down"
                End If
                If s_before = "downleft" And s_after = "downleft" Then
                    JumpOnWayIm(i) = "downleft"
                End If
            Next
        End If
    End Sub

    Function M_Arriba(ByVal n As Integer)
        Dim r As Integer
        r = n - columnas
        Return (r)
    End Function

    Function M_Abajo(ByVal n As Integer)
        Dim r As Integer
        r = n + columnas
        Return (r)
    End Function

    Function M_Derecha(ByVal n As Integer)
        Dim r As Integer
        r = n + 1
        Return (r)
    End Function

    Function M_Izquierda(ByVal n As Integer)
        Dim r As Integer
        r = n - 1
        Return (r)
    End Function

End Class