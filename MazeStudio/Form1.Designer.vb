﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.LDiseñar = New System.Windows.Forms.Label()
        Me.LResolver = New System.Windows.Forms.Label()
        Me.TimerIni = New System.Windows.Forms.Timer(Me.components)
        Me.ProgressBarIni = New System.Windows.Forms.ProgressBar()
        Me.SuspendLayout()
        '
        'LDiseñar
        '
        Me.LDiseñar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LDiseñar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LDiseñar.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LDiseñar.Location = New System.Drawing.Point(23, 43)
        Me.LDiseñar.Name = "LDiseñar"
        Me.LDiseñar.Size = New System.Drawing.Size(440, 44)
        Me.LDiseñar.TabIndex = 0
        Me.LDiseñar.Text = "Diseñar un Laberinto"
        Me.LDiseñar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LResolver
        '
        Me.LResolver.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LResolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LResolver.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LResolver.Location = New System.Drawing.Point(22, 132)
        Me.LResolver.Name = "LResolver"
        Me.LResolver.Size = New System.Drawing.Size(440, 44)
        Me.LResolver.TabIndex = 1
        Me.LResolver.Text = "Resolver un Laberinto"
        Me.LResolver.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TimerIni
        '
        Me.TimerIni.Interval = 5
        '
        'ProgressBarIni
        '
        Me.ProgressBarIni.Location = New System.Drawing.Point(-1, 0)
        Me.ProgressBarIni.Name = "ProgressBarIni"
        Me.ProgressBarIni.Size = New System.Drawing.Size(486, 5)
        Me.ProgressBarIni.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(484, 221)
        Me.Controls.Add(Me.ProgressBarIni)
        Me.Controls.Add(Me.LResolver)
        Me.Controls.Add(Me.LDiseñar)
        Me.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(500, 260)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(500, 260)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Maze Studio"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LDiseñar As Label
    Friend WithEvents LResolver As Label
    Friend WithEvents TimerIni As Timer
    Friend WithEvents ProgressBarIni As ProgressBar
End Class
