﻿Public Class BeforeDesign
    Dim sonido_boton As System.Media.SoundPlayer
    Dim sonido_cambiar As System.Media.SoundPlayer
    Dim sonido_error As System.Media.SoundPlayer
    Dim action, filas, columnas As Integer
    Dim cadena As String

    Private Sub BeforeDesign_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        sonido_boton = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\button.wav")
        sonido_cambiar = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\change.wav")
        sonido_error = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\error.wav")
    End Sub

    Private Sub LAbrir_Click(sender As Object, e As EventArgs) Handles LAbrir.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_cambiar.Play()
        End If
        action = 1
        'Mostrar
        LRuta.Visible = True
        TBRuta.Visible = True
        LGetRuta.Visible = True
        LIniciar.Visible = True
        'Ocultar
        LFilas.Visible = False
        CBColumnas.Visible = False
        LColumnas.Visible = False
        CBFilas.Visible = False
        LNombre.Visible = False
        TBNombre.Visible = False
        LGetRuta.Focus()
    End Sub

    Private Sub LAbrir_MouseEnter(sender As Object, e As EventArgs) Handles LAbrir.MouseEnter
        LAbrir.ForeColor = Color.DodgerBlue
    End Sub

    Private Sub LAbrir_MouseLeave(sender As Object, e As EventArgs) Handles LAbrir.MouseLeave
        LAbrir.ForeColor = Color.DimGray
    End Sub

    Private Sub LNuevo_Click(sender As Object, e As EventArgs) Handles LNuevo.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_cambiar.Play()
        End If
        action = 2
        'Mostrar
        LFilas.Visible = True
        CBColumnas.Visible = True
        LColumnas.Visible = True
        CBFilas.Visible = True
        LNombre.Visible = True
        TBNombre.Visible = True
        LIniciar.Visible = True
        'Ocultar
        LRuta.Visible = False
        TBRuta.Visible = False
        LGetRuta.Visible = False
    End Sub

    Private Sub LNuevo_MouseEnter(sender As Object, e As EventArgs) Handles LNuevo.MouseEnter
        LNuevo.ForeColor = Color.DodgerBlue
    End Sub

    Private Sub LNuevo_MouseLeave(sender As Object, e As EventArgs) Handles LNuevo.MouseLeave
        LNuevo.ForeColor = Color.DimGray
    End Sub

    Private Sub LGetRuta_Click(sender As Object, e As EventArgs) Handles LGetRuta.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_boton.Play()
        End If
        OpenFileDialogFile.InitialDirectory = My.Application.Info.DirectoryPath
        If OpenFileDialogFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
            TBRuta.Text = IO.Path.GetFullPath(OpenFileDialogFile.FileName)
            RTBLoader.LoadFile(TBRuta.Text, RichTextBoxStreamType.PlainText)
            filas = RTBLoader.Lines(0)
            columnas = RTBLoader.Lines(1)
            cadena = RTBLoader.Lines(2)
        End If
    End Sub

    Private Sub TBNombre_TextChanged(sender As Object, e As EventArgs) Handles TBNombre.TextChanged

    End Sub

    Private Sub TBNombre_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TBNombre.KeyPress
        Dim filtro As String
        filtro = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_"
        If InStr(filtro, e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub LIniciar_Click(sender As Object, e As EventArgs) Handles LIniciar.Click
        Select Case action
            Case 1
                If Len(TBRuta.Text) = 0 Then
                    MsgBox("Archivo de laberinto no seleccionado.", MsgBoxStyle.Exclamation, "Maze Studio")
                Else
                    If Len(cadena) = (filas * columnas) Then
                        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
                            sonido_cambiar.Play()
                        End If
                        Design.RTBGetPath.Text = TBRuta.Text
                        MsgBox("Archivo de laberinto compatible:" & vbCrLf & TBRuta.Text, MsgBoxStyle.Information, "Maze Studio")
                        Design.Show()
                        Me.Close()
                    Else
                        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
                            sonido_error.Play()
                        End If
                        TBRuta.Text = ""
                        RTBLoader.Text = ""
                        MsgBox("Archivo de laberinto no compatible.", MsgBoxStyle.Exclamation, "Maze Studio")
                    End If
                End If
            Case 2
                If Len(TBNombre.Text) >= 1 And Len(CBColumnas.Text) >= 1 And Len(CBFilas.Text) >= 1 Then
                    If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
                        sonido_cambiar.Play()
                    End If
                    Dim str As String
                    str = CBFilas.Text & vbCrLf & CBColumnas.Text & vbCrLf & ""
                    For i = 1 To (CBColumnas.Text * CBFilas.Text) Step 1
                        str = str & "0"
                    Next
                    Design.RTBGetPath.Text = My.Application.Info.DirectoryPath & "\" & TBNombre.Text & ".lbr"
                    My.Computer.FileSystem.WriteAllText(My.Application.Info.DirectoryPath & "\" & TBNombre.Text & ".lbr", str, False)
                    MsgBox("El archivo se guardó como:" & vbCrLf & My.Application.Info.DirectoryPath & "\" & TBNombre.Text & ".lbr", MsgBoxStyle.Information, "Maze Studio")
                    Design.Show()
                    Me.Close()
                Else
                    If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
                        sonido_error.Play()
                    End If
                    MsgBox("Error al iniciar nuevo laberinto:" & vbCrLf & "*No hay nombre para el laberinto" & vbCrLf & "*No hay número de filas y/o columnas.", MsgBoxStyle.Exclamation, "Maze Studio")
                End If
        End Select
    End Sub

    Private Sub TBRuta_TextChanged(sender As Object, e As EventArgs) Handles TBRuta.TextChanged

    End Sub

    Private Sub CBFilas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBColumnas.SelectedIndexChanged
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_boton.Play()
        End If
    End Sub

    Private Sub CBColumnas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBFilas.SelectedIndexChanged
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_boton.Play()
        End If
    End Sub

End Class