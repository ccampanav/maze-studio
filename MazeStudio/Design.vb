﻿Public Class Design
    Dim sonido_celda As System.Media.SoundPlayer
    Dim sonido_boton As System.Media.SoundPlayer
    Dim sonido_cambiar As System.Media.SoundPlayer
    Dim sonido_error As System.Media.SoundPlayer
    Dim sizeT, filas, columnas, nceldas, celda_tam, pointX, pointY As Integer
    Dim nEntrada, nSalida As Integer
    Dim str(1600) As Byte
    Dim action As String

    Public Sub Design_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        sonido_celda = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\cell.wav")
        sonido_boton = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\button.wav")
        sonido_cambiar = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\change.wav")
        sonido_error = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\error.wav")
        sizeT = 640 : action = "" : nEntrada = 8 : nSalida = 9
        If Not My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            LSonido.Text = "Sonido: Desactivado"
            LSonido.ForeColor = Color.DimGray
        Else
            LSonido.Text = "Sonido: Activado"
            LSonido.ForeColor = Color.DodgerBlue
        End If
        RTBLoader.LoadFile(RTBGetPath.Text, RichTextBoxStreamType.PlainText)
        filas = RTBLoader.Lines(0)
        columnas = RTBLoader.Lines(1)
        nceldas = filas * columnas
        For i = 1 To nceldas Step 1
            str(i) = Microsoft.VisualBasic.Mid(RTBLoader.Lines(2), i, 1)
        Next
        Dim TTN As New ToolTip()
        TTN.IsBalloon = False
        TTN.InitialDelay = 1
        TTN.ReshowDelay = 1
        TTN.BackColor = Color.White
        TTN.ForeColor = Color.DimGray
        TTN.AutoPopDelay = 300000
        TTN.UseAnimation = True
        TTN.UseFading = True
        If filas > columnas Then
            celda_tam = Fix(sizeT / filas)
        Else
            celda_tam = Fix(sizeT / columnas)
        End If
        If filas = columnas Then
            celda_tam = Fix(sizeT / filas)
        End If
        If Not celda_tam Mod 2 = 0 Then
            celda_tam -= 1
        End If
        Dim aux1 As Integer
        pointX = 298 : pointY = 16 : aux1 = 1
        For i = 1 To nceldas Step 1
            Dim newCell As System.Windows.Forms.PictureBox
            newCell = New Windows.Forms.PictureBox
            newCell.Name = "Celda_" & i
            newCell.Left = pointX
            newCell.Top = pointY
            newCell.Width = celda_tam
            newCell.Height = celda_tam
            newCell.Cursor = Cursors.Hand
            newCell.BorderStyle = BorderStyle.FixedSingle
            newCell.SizeMode = PictureBoxSizeMode.StretchImage
            newCell.BackgroundImageLayout = ImageLayout.Stretch
            Select Case str(i)
                Case 0
                    newCell.BackgroundImage = My.Resources.celda_0
                Case 1
                    newCell.BackgroundImage = My.Resources.celda_1
                Case nEntrada
                    newCell.BackgroundImage = My.Resources.Celda_Entrada
                Case nSalida
                    newCell.BackgroundImage = My.Resources.Celda_Salida
            End Select
            TTN.SetToolTip(newCell, newCell.Name)
            newCell.Enabled = True
            newCell.Visible = True
            AddHandler newCell.MouseEnter, AddressOf newCell_MouseEnter
            AddHandler newCell.MouseLeave, AddressOf newCell_MouseLeave
            AddHandler newCell.MouseClick, AddressOf newCell_MouseClick
            Me.Controls.Add(newCell)
            aux1 += 1 : pointX += celda_tam
            If aux1 = columnas + 1 Then
                aux1 = 1 : pointY += celda_tam : pointX = 298
            End If
        Next
        Call PBCelda_Llenar_Click(Nothing, Nothing)
    End Sub

    Private Sub newCell_MouseEnter(ByVal sender As Object,
    ByVal e As System.EventArgs)
        Select Case action
            Case "borrar"
                CType(sender, System.Windows.Forms.PictureBox).Image = My.Resources.celda_0
            Case "llenar"
                CType(sender, System.Windows.Forms.PictureBox).Image = My.Resources.celda_1
            Case "entrada"
                CType(sender, System.Windows.Forms.PictureBox).Image = My.Resources.Celda_Entrada
            Case "salida"
                CType(sender, System.Windows.Forms.PictureBox).Image = My.Resources.Celda_Salida
        End Select
    End Sub

    Private Sub newCell_MouseLeave(ByVal sender As Object,
    ByVal e As System.EventArgs)
        Select Case action
            Case "borrar"
                CType(sender, System.Windows.Forms.PictureBox).Image = Nothing
            Case "llenar"
                CType(sender, System.Windows.Forms.PictureBox).Image = Nothing
            Case "entrada"
                CType(sender, System.Windows.Forms.PictureBox).Image = Nothing
            Case "salida"
                CType(sender, System.Windows.Forms.PictureBox).Image = Nothing
        End Select
    End Sub

    Private Sub newCell_MouseClick(ByVal sender As Object,
    ByVal e As System.EventArgs)
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_celda.Play()
        End If
        Dim n As Integer
        n = Microsoft.VisualBasic.Mid(CType(sender, System.Windows.Forms.PictureBox).Name, 7, Len(CType(sender, System.Windows.Forms.PictureBox).Name) - 6)
        Select Case action
            Case "borrar"
                str(n) = 0
                CType(sender, System.Windows.Forms.PictureBox).BackgroundImage = My.Resources.celda_0
            Case "llenar"
                str(n) = 1
                CType(sender, System.Windows.Forms.PictureBox).BackgroundImage = My.Resources.celda_1
            Case "entrada"
                str(n) = nEntrada
                CType(sender, System.Windows.Forms.PictureBox).BackgroundImage = My.Resources.Celda_Entrada
            Case "salida"
                str(n) = nSalida
                CType(sender, System.Windows.Forms.PictureBox).BackgroundImage = My.Resources.Celda_Salida
        End Select
    End Sub

    Private Sub SaveMaze()
        Dim strFinal As String
        strFinal = ""
        For i = 1 To nceldas Step 1
            strFinal = strFinal & str(i)
        Next
        My.Computer.FileSystem.WriteAllText(RTBGetPath.Text, filas & vbCrLf & columnas & vbCrLf & strFinal, False)
    End Sub

    Function CheckMaze()
        Dim n As String
        Dim numEntradas, numSalidas As Integer
        numEntradas = 0 : numSalidas = 0 : n = ""
        For i = 1 To nceldas Step 1
            Select Case str(i)
                Case nEntrada
                    numEntradas += 1
                Case nSalida
                    numSalidas += 1
            End Select
        Next
        If numEntradas = 1 And numSalidas = 1 Then
            n = "no_error"
        Else
            n = "error"
        End If
        Return (n)
    End Function

    Private Sub LGuardar_Click(sender As Object, e As EventArgs) Handles LGuardar.Click
        Select Case CheckMaze()
            Case "no_error"
                Call SaveMaze()
                If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
                    sonido_cambiar.Play()
                End If
                MsgBox("Laberinto guardado correctamente.", MsgBoxStyle.Information, "Maze Studio")
            Case "error"
                If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
                    sonido_error.Play()
                End If
                MsgBox("Error al guardar el laberinto:" & vbCrLf & "Sólo debe existir una entrada y una salida en el laberinto.", MsgBoxStyle.Exclamation, "Maze Studio")
        End Select
    End Sub

    Private Sub LSonido_Click(sender As Object, e As EventArgs) Handles LSonido.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            My.Computer.FileSystem.DeleteFile("conf\sonido.txt")
            LSonido.Text = "Sonido: Desactivado"
            LSonido.ForeColor = Color.DimGray
            sonido_boton.Play()
        Else
            My.Computer.FileSystem.WriteAllText("conf\sonido.txt", "1", False)
            LSonido.Text = "Sonido: Activado"
            LSonido.ForeColor = Color.DodgerBlue
        End If
    End Sub

    Private Sub LVaciarTodo_Click(sender As Object, e As EventArgs) Handles LVaciarTodo.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_boton.Play()
        End If
        Dim r = MsgBox("¿Realmente quieres vaciar todo el laberinto?", MsgBoxStyle.YesNo, "Maze Studio")
        If r = MsgBoxResult.Yes Then
            Me.Enabled = False
            For i = 1 To nceldas Step 1
                str(i) = 0
                For Each cell In Me.Controls
                    If cell.name = "Celda_" & i Then
                        cell.BackgroundImage = My.Resources.celda_0
                    End If
                Next
            Next
            Me.Enabled = True
        End If
    End Sub

    Private Sub LLlenarTodo_Click(sender As Object, e As EventArgs) Handles LLlenarTodo.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_boton.Play()
        End If
        Dim r = MsgBox("¿Realmente quieres llenar todo el laberinto?", MsgBoxStyle.YesNo, "Maze Studio")
        If r = MsgBoxResult.Yes Then
            Me.Enabled = False
            For i = 1 To nceldas Step 1
                str(i) = 1
                For Each cell In Me.Controls
                    If cell.name = "Celda_" & i Then
                        cell.BackgroundImage = My.Resources.celda_1
                    End If
                Next
            Next
            Me.Enabled = True
        End If
    End Sub

    Private Sub LEntrada_Click(sender As Object, e As EventArgs) Handles LEntrada.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_boton.Play()
        End If
        action = "entrada"
    End Sub

    Private Sub LSalida_Click(sender As Object, e As EventArgs) Handles LSalida.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_boton.Play()
        End If
        action = "salida"
    End Sub

    Private Sub PBCelda_Borrar_Click(sender As Object, e As EventArgs) Handles PBCelda_Borrar.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_boton.Play()
        End If
        PBCelda_Borrar.BorderStyle = BorderStyle.FixedSingle
        PBCelda_Llenar.BorderStyle = BorderStyle.None
        LUsandoColor.Text = "Usando color: Blanco"
        action = "borrar"
    End Sub

    Private Sub PBCelda_Llenar_Click(sender As Object, e As EventArgs) Handles PBCelda_Llenar.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_boton.Play()
        End If
        PBCelda_Borrar.BorderStyle = BorderStyle.None
        PBCelda_Llenar.BorderStyle = BorderStyle.FixedSingle
        LUsandoColor.Text = "Usando color: Gris"
        action = "llenar"
    End Sub

End Class