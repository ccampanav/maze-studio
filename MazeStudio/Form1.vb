﻿Public Class Form1
    Dim action As Integer
    Dim sonido_cambiar As System.Media.SoundPlayer

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        sonido_cambiar = New System.Media.SoundPlayer(My.Application.Info.DirectoryPath & "\sonidos\change.wav")
    End Sub

    Private Sub LDiseñar_Click(sender As Object, e As EventArgs) Handles LDiseñar.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_cambiar.Play()
        End If
        action = 1
        LDiseñar.Enabled = False
        LResolver.Enabled = False
        TimerIni.Enabled = True
    End Sub

    Private Sub LDiseñar_MouseEnter(sender As Object, e As EventArgs) Handles LDiseñar.MouseEnter
        LDiseñar.ForeColor = Color.DodgerBlue
    End Sub

    Private Sub LDiseñar_MouseLeave(sender As Object, e As EventArgs) Handles LDiseñar.MouseLeave
        LDiseñar.ForeColor = Color.DimGray
    End Sub

    Private Sub LResolver_Click(sender As Object, e As EventArgs) Handles LResolver.Click
        If My.Computer.FileSystem.FileExists("conf/sonido.txt") Then
            sonido_cambiar.Play()
        End If
        action = 2
        LDiseñar.Enabled = False
        LResolver.Enabled = False
        TimerIni.Enabled = True
    End Sub

    Private Sub LResolver_MouseEnter(sender As Object, e As EventArgs) Handles LResolver.MouseEnter
        LResolver.ForeColor = Color.DodgerBlue
    End Sub

    Private Sub LResolver_MouseLeave(sender As Object, e As EventArgs) Handles LResolver.MouseLeave
        LResolver.ForeColor = Color.DimGray
    End Sub

    Private Sub TimerIni_Tick(sender As Object, e As EventArgs) Handles TimerIni.Tick
        ProgressBarIni.Increment(1)
        If ProgressBarIni.Value = 100 Then
            Select Case action
                Case 1
                    BeforeDesign.Show()
                Case 2
                    BeforeSolve.Show()
            End Select
            Me.Close()
        End If
    End Sub

End Class
