﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Solve
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Solve))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RTBLoader = New System.Windows.Forms.RichTextBox()
        Me.RTBGetPath = New System.Windows.Forms.RichTextBox()
        Me.LSonido = New System.Windows.Forms.Label()
        Me.LEstado = New System.Windows.Forms.Label()
        Me.LIniciar = New System.Windows.Forms.Label()
        Me.LPausar = New System.Windows.Forms.Label()
        Me.LDetener = New System.Windows.Forms.Label()
        Me.LCeldaActual = New System.Windows.Forms.Label()
        Me.LCeldasVisitadas = New System.Windows.Forms.Label()
        Me.TimerAction = New System.Windows.Forms.Timer(Me.components)
        Me.PBAction = New System.Windows.Forms.ProgressBar()
        Me.PBBorderDown = New System.Windows.Forms.PictureBox()
        Me.PBBorderUp = New System.Windows.Forms.PictureBox()
        Me.PBBorderRight = New System.Windows.Forms.PictureBox()
        Me.PBSpinner = New System.Windows.Forms.PictureBox()
        Me.PBBorderLeft = New System.Windows.Forms.PictureBox()
        Me.PBLine = New System.Windows.Forms.PictureBox()
        CType(Me.PBBorderDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBBorderUp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBBorderRight, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBSpinner, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBBorderLeft, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBLine, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label1.Location = New System.Drawing.Point(11, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(278, 49)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "Solucionador"
        '
        'RTBLoader
        '
        Me.RTBLoader.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RTBLoader.Cursor = System.Windows.Forms.Cursors.Default
        Me.RTBLoader.DetectUrls = False
        Me.RTBLoader.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RTBLoader.ForeColor = System.Drawing.Color.DimGray
        Me.RTBLoader.Location = New System.Drawing.Point(99, 7)
        Me.RTBLoader.MaxLength = 2000
        Me.RTBLoader.Name = "RTBLoader"
        Me.RTBLoader.ReadOnly = True
        Me.RTBLoader.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.RTBLoader.Size = New System.Drawing.Size(83, 10)
        Me.RTBLoader.TabIndex = 32
        Me.RTBLoader.Text = ""
        Me.RTBLoader.Visible = False
        '
        'RTBGetPath
        '
        Me.RTBGetPath.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RTBGetPath.Cursor = System.Windows.Forms.Cursors.Default
        Me.RTBGetPath.DetectUrls = False
        Me.RTBGetPath.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RTBGetPath.ForeColor = System.Drawing.Color.DimGray
        Me.RTBGetPath.Location = New System.Drawing.Point(8, 7)
        Me.RTBGetPath.MaxLength = 300
        Me.RTBGetPath.Multiline = False
        Me.RTBGetPath.Name = "RTBGetPath"
        Me.RTBGetPath.ReadOnly = True
        Me.RTBGetPath.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.RTBGetPath.Size = New System.Drawing.Size(83, 10)
        Me.RTBGetPath.TabIndex = 31
        Me.RTBGetPath.Text = ""
        Me.RTBGetPath.Visible = False
        '
        'LSonido
        '
        Me.LSonido.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LSonido.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSonido.ForeColor = System.Drawing.Color.DimGray
        Me.LSonido.Location = New System.Drawing.Point(58, 135)
        Me.LSonido.Name = "LSonido"
        Me.LSonido.Size = New System.Drawing.Size(180, 20)
        Me.LSonido.TabIndex = 33
        Me.LSonido.Text = "Sonido: Desactivado"
        Me.LSonido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LEstado
        '
        Me.LEstado.Cursor = System.Windows.Forms.Cursors.Default
        Me.LEstado.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LEstado.ForeColor = System.Drawing.Color.DimGray
        Me.LEstado.Location = New System.Drawing.Point(22, 477)
        Me.LEstado.Name = "LEstado"
        Me.LEstado.Size = New System.Drawing.Size(250, 20)
        Me.LEstado.TabIndex = 35
        Me.LEstado.Text = "Estado: Sin iniciar"
        Me.LEstado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LIniciar
        '
        Me.LIniciar.AutoSize = True
        Me.LIniciar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LIniciar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LIniciar.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LIniciar.Location = New System.Drawing.Point(116, 258)
        Me.LIniciar.Name = "LIniciar"
        Me.LIniciar.Size = New System.Drawing.Size(57, 21)
        Me.LIniciar.TabIndex = 36
        Me.LIniciar.Text = "Iniciar"
        Me.LIniciar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LPausar
        '
        Me.LPausar.AutoSize = True
        Me.LPausar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LPausar.Enabled = False
        Me.LPausar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LPausar.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LPausar.Location = New System.Drawing.Point(59, 211)
        Me.LPausar.Name = "LPausar"
        Me.LPausar.Size = New System.Drawing.Size(62, 21)
        Me.LPausar.TabIndex = 37
        Me.LPausar.Text = "Pausar"
        Me.LPausar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LDetener
        '
        Me.LDetener.AutoSize = True
        Me.LDetener.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LDetener.Enabled = False
        Me.LDetener.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LDetener.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LDetener.Location = New System.Drawing.Point(173, 211)
        Me.LDetener.Name = "LDetener"
        Me.LDetener.Size = New System.Drawing.Size(74, 21)
        Me.LDetener.TabIndex = 39
        Me.LDetener.Text = "Detener"
        Me.LDetener.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LCeldaActual
        '
        Me.LCeldaActual.Cursor = System.Windows.Forms.Cursors.Default
        Me.LCeldaActual.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCeldaActual.ForeColor = System.Drawing.Color.DimGray
        Me.LCeldaActual.Location = New System.Drawing.Point(22, 510)
        Me.LCeldaActual.Name = "LCeldaActual"
        Me.LCeldaActual.Size = New System.Drawing.Size(250, 20)
        Me.LCeldaActual.TabIndex = 40
        Me.LCeldaActual.Text = "Celda actual: 0"
        Me.LCeldaActual.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LCeldasVisitadas
        '
        Me.LCeldasVisitadas.Cursor = System.Windows.Forms.Cursors.Default
        Me.LCeldasVisitadas.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCeldasVisitadas.ForeColor = System.Drawing.Color.DimGray
        Me.LCeldasVisitadas.Location = New System.Drawing.Point(22, 543)
        Me.LCeldasVisitadas.Name = "LCeldasVisitadas"
        Me.LCeldasVisitadas.Size = New System.Drawing.Size(250, 20)
        Me.LCeldasVisitadas.TabIndex = 45
        Me.LCeldasVisitadas.Text = "Celdas visitadas: 0"
        Me.LCeldasVisitadas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TimerAction
        '
        Me.TimerAction.Interval = 1
        '
        'PBAction
        '
        Me.PBAction.Location = New System.Drawing.Point(205, 8)
        Me.PBAction.Name = "PBAction"
        Me.PBAction.Size = New System.Drawing.Size(50, 3)
        Me.PBAction.TabIndex = 49
        Me.PBAction.Visible = False
        '
        'PBBorderDown
        '
        Me.PBBorderDown.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.PBBorderDown.Location = New System.Drawing.Point(297, 656)
        Me.PBBorderDown.Name = "PBBorderDown"
        Me.PBBorderDown.Size = New System.Drawing.Size(642, 1)
        Me.PBBorderDown.TabIndex = 44
        Me.PBBorderDown.TabStop = False
        '
        'PBBorderUp
        '
        Me.PBBorderUp.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.PBBorderUp.Location = New System.Drawing.Point(297, 15)
        Me.PBBorderUp.Name = "PBBorderUp"
        Me.PBBorderUp.Size = New System.Drawing.Size(642, 1)
        Me.PBBorderUp.TabIndex = 43
        Me.PBBorderUp.TabStop = False
        '
        'PBBorderRight
        '
        Me.PBBorderRight.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.PBBorderRight.Location = New System.Drawing.Point(938, 15)
        Me.PBBorderRight.Name = "PBBorderRight"
        Me.PBBorderRight.Size = New System.Drawing.Size(1, 642)
        Me.PBBorderRight.TabIndex = 42
        Me.PBBorderRight.TabStop = False
        '
        'PBSpinner
        '
        Me.PBSpinner.Image = CType(resources.GetObject("PBSpinner.Image"), System.Drawing.Image)
        Me.PBSpinner.Location = New System.Drawing.Point(97, 333)
        Me.PBSpinner.Name = "PBSpinner"
        Me.PBSpinner.Size = New System.Drawing.Size(100, 100)
        Me.PBSpinner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBSpinner.TabIndex = 41
        Me.PBSpinner.TabStop = False
        Me.PBSpinner.Visible = False
        '
        'PBBorderLeft
        '
        Me.PBBorderLeft.BackColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.PBBorderLeft.Location = New System.Drawing.Point(297, 15)
        Me.PBBorderLeft.Name = "PBBorderLeft"
        Me.PBBorderLeft.Size = New System.Drawing.Size(1, 642)
        Me.PBBorderLeft.TabIndex = 34
        Me.PBBorderLeft.TabStop = False
        '
        'PBLine
        '
        Me.PBLine.BackColor = System.Drawing.Color.DodgerBlue
        Me.PBLine.Location = New System.Drawing.Point(24, 95)
        Me.PBLine.Name = "PBLine"
        Me.PBLine.Size = New System.Drawing.Size(250, 2)
        Me.PBLine.TabIndex = 29
        Me.PBLine.TabStop = False
        '
        'Solve
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(954, 671)
        Me.Controls.Add(Me.PBAction)
        Me.Controls.Add(Me.LCeldasVisitadas)
        Me.Controls.Add(Me.PBBorderDown)
        Me.Controls.Add(Me.PBBorderUp)
        Me.Controls.Add(Me.PBBorderRight)
        Me.Controls.Add(Me.PBSpinner)
        Me.Controls.Add(Me.LCeldaActual)
        Me.Controls.Add(Me.LDetener)
        Me.Controls.Add(Me.LPausar)
        Me.Controls.Add(Me.LIniciar)
        Me.Controls.Add(Me.LEstado)
        Me.Controls.Add(Me.PBBorderLeft)
        Me.Controls.Add(Me.LSonido)
        Me.Controls.Add(Me.RTBLoader)
        Me.Controls.Add(Me.RTBGetPath)
        Me.Controls.Add(Me.PBLine)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(970, 710)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(970, 710)
        Me.Name = "Solve"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Maze Studio"
        CType(Me.PBBorderDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBBorderUp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBBorderRight, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBSpinner, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBBorderLeft, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBLine, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PBLine As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents RTBLoader As RichTextBox
    Friend WithEvents RTBGetPath As RichTextBox
    Friend WithEvents LSonido As Label
    Friend WithEvents PBBorderLeft As PictureBox
    Friend WithEvents LEstado As Label
    Friend WithEvents LIniciar As Label
    Friend WithEvents LPausar As Label
    Friend WithEvents LDetener As Label
    Friend WithEvents LCeldaActual As Label
    Friend WithEvents PBSpinner As PictureBox
    Friend WithEvents PBBorderRight As PictureBox
    Friend WithEvents PBBorderUp As PictureBox
    Friend WithEvents PBBorderDown As PictureBox
    Friend WithEvents LCeldasVisitadas As Label
    Friend WithEvents TimerAction As Timer
    Friend WithEvents PBAction As ProgressBar
End Class
