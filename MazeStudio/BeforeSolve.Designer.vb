﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BeforeSolve
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BeforeSolve))
        Me.LAbrir = New System.Windows.Forms.Label()
        Me.LGetRuta = New System.Windows.Forms.Label()
        Me.TBRuta = New System.Windows.Forms.TextBox()
        Me.LRuta = New System.Windows.Forms.Label()
        Me.LIniciar = New System.Windows.Forms.Label()
        Me.OpenFileDialogFile = New System.Windows.Forms.OpenFileDialog()
        Me.RTBLoader = New System.Windows.Forms.RichTextBox()
        Me.SuspendLayout()
        '
        'LAbrir
        '
        Me.LAbrir.Cursor = System.Windows.Forms.Cursors.Default
        Me.LAbrir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LAbrir.Font = New System.Drawing.Font("Century Gothic", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LAbrir.Location = New System.Drawing.Point(77, 28)
        Me.LAbrir.Name = "LAbrir"
        Me.LAbrir.Size = New System.Drawing.Size(400, 42)
        Me.LAbrir.TabIndex = 2
        Me.LAbrir.Text = "Abrir archivo de laberinto"
        Me.LAbrir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LGetRuta
        '
        Me.LGetRuta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LGetRuta.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LGetRuta.Location = New System.Drawing.Point(511, 126)
        Me.LGetRuta.Name = "LGetRuta"
        Me.LGetRuta.Size = New System.Drawing.Size(26, 23)
        Me.LGetRuta.TabIndex = 19
        Me.LGetRuta.Text = "..."
        Me.LGetRuta.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TBRuta
        '
        Me.TBRuta.BackColor = System.Drawing.Color.White
        Me.TBRuta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBRuta.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBRuta.ForeColor = System.Drawing.Color.DimGray
        Me.TBRuta.Location = New System.Drawing.Point(175, 126)
        Me.TBRuta.MaxLength = 1000
        Me.TBRuta.Name = "TBRuta"
        Me.TBRuta.ReadOnly = True
        Me.TBRuta.Size = New System.Drawing.Size(330, 23)
        Me.TBRuta.TabIndex = 18
        Me.TBRuta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LRuta
        '
        Me.LRuta.AutoSize = True
        Me.LRuta.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LRuta.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LRuta.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LRuta.Location = New System.Drawing.Point(28, 127)
        Me.LRuta.Name = "LRuta"
        Me.LRuta.Size = New System.Drawing.Size(143, 21)
        Me.LRuta.TabIndex = 17
        Me.LRuta.Text = "Ruta del archivo:"
        Me.LRuta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LIniciar
        '
        Me.LIniciar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LIniciar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LIniciar.Font = New System.Drawing.Font("Century Gothic", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LIniciar.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LIniciar.Location = New System.Drawing.Point(224, 195)
        Me.LIniciar.Name = "LIniciar"
        Me.LIniciar.Size = New System.Drawing.Size(120, 36)
        Me.LIniciar.TabIndex = 20
        Me.LIniciar.Text = "Iniciar"
        Me.LIniciar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'OpenFileDialogFile
        '
        Me.OpenFileDialogFile.Filter = "Laberintos |*.lbr"
        Me.OpenFileDialogFile.Title = "Selecciona un archivo de laberinto"
        '
        'RTBLoader
        '
        Me.RTBLoader.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RTBLoader.Location = New System.Drawing.Point(475, 6)
        Me.RTBLoader.MaxLength = 300
        Me.RTBLoader.Name = "RTBLoader"
        Me.RTBLoader.Size = New System.Drawing.Size(83, 12)
        Me.RTBLoader.TabIndex = 21
        Me.RTBLoader.Text = ""
        Me.RTBLoader.Visible = False
        '
        'BeforeSolve
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(564, 261)
        Me.Controls.Add(Me.RTBLoader)
        Me.Controls.Add(Me.LIniciar)
        Me.Controls.Add(Me.LGetRuta)
        Me.Controls.Add(Me.TBRuta)
        Me.Controls.Add(Me.LRuta)
        Me.Controls.Add(Me.LAbrir)
        Me.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(580, 300)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(580, 300)
        Me.Name = "BeforeSolve"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Maze Studio"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LAbrir As Label
    Friend WithEvents LGetRuta As Label
    Friend WithEvents TBRuta As TextBox
    Friend WithEvents LRuta As Label
    Friend WithEvents LIniciar As Label
    Friend WithEvents OpenFileDialogFile As OpenFileDialog
    Friend WithEvents RTBLoader As RichTextBox
End Class
