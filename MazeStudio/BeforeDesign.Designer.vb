﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BeforeDesign
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BeforeDesign))
        Me.LAbrir = New System.Windows.Forms.Label()
        Me.LNuevo = New System.Windows.Forms.Label()
        Me.LFilas = New System.Windows.Forms.Label()
        Me.LColumnas = New System.Windows.Forms.Label()
        Me.LIniciar = New System.Windows.Forms.Label()
        Me.LRuta = New System.Windows.Forms.Label()
        Me.TBRuta = New System.Windows.Forms.TextBox()
        Me.TBNombre = New System.Windows.Forms.TextBox()
        Me.LNombre = New System.Windows.Forms.Label()
        Me.LGetRuta = New System.Windows.Forms.Label()
        Me.OpenFileDialogFile = New System.Windows.Forms.OpenFileDialog()
        Me.RTBLoader = New System.Windows.Forms.RichTextBox()
        Me.CBColumnas = New System.Windows.Forms.ComboBox()
        Me.CBFilas = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'LAbrir
        '
        Me.LAbrir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LAbrir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LAbrir.Font = New System.Drawing.Font("Century Gothic", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LAbrir.Location = New System.Drawing.Point(77, 28)
        Me.LAbrir.Name = "LAbrir"
        Me.LAbrir.Size = New System.Drawing.Size(400, 42)
        Me.LAbrir.TabIndex = 1
        Me.LAbrir.Text = "Abrir archivo de laberinto"
        Me.LAbrir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LNuevo
        '
        Me.LNuevo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LNuevo.Font = New System.Drawing.Font("Century Gothic", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LNuevo.Location = New System.Drawing.Point(131, 88)
        Me.LNuevo.Name = "LNuevo"
        Me.LNuevo.Size = New System.Drawing.Size(300, 44)
        Me.LNuevo.TabIndex = 2
        Me.LNuevo.Text = "Nuevo laberinto"
        Me.LNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LFilas
        '
        Me.LFilas.AutoSize = True
        Me.LFilas.Location = New System.Drawing.Point(180, 173)
        Me.LFilas.Name = "LFilas"
        Me.LFilas.Size = New System.Drawing.Size(42, 20)
        Me.LFilas.TabIndex = 7
        Me.LFilas.Text = "Filas:"
        Me.LFilas.Visible = False
        '
        'LColumnas
        '
        Me.LColumnas.AutoSize = True
        Me.LColumnas.Location = New System.Drawing.Point(138, 209)
        Me.LColumnas.Name = "LColumnas"
        Me.LColumnas.Size = New System.Drawing.Size(85, 20)
        Me.LColumnas.TabIndex = 8
        Me.LColumnas.Text = "Columnas:"
        Me.LColumnas.Visible = False
        '
        'LIniciar
        '
        Me.LIniciar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LIniciar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LIniciar.Font = New System.Drawing.Font("Century Gothic", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LIniciar.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LIniciar.Location = New System.Drawing.Point(224, 302)
        Me.LIniciar.Name = "LIniciar"
        Me.LIniciar.Size = New System.Drawing.Size(120, 36)
        Me.LIniciar.TabIndex = 9
        Me.LIniciar.Text = "Iniciar"
        Me.LIniciar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LIniciar.Visible = False
        '
        'LRuta
        '
        Me.LRuta.AutoSize = True
        Me.LRuta.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LRuta.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LRuta.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LRuta.Location = New System.Drawing.Point(26, 192)
        Me.LRuta.Name = "LRuta"
        Me.LRuta.Size = New System.Drawing.Size(143, 21)
        Me.LRuta.TabIndex = 10
        Me.LRuta.Text = "Ruta del archivo:"
        Me.LRuta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LRuta.Visible = False
        '
        'TBRuta
        '
        Me.TBRuta.BackColor = System.Drawing.Color.White
        Me.TBRuta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBRuta.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBRuta.ForeColor = System.Drawing.Color.DimGray
        Me.TBRuta.Location = New System.Drawing.Point(173, 191)
        Me.TBRuta.MaxLength = 1000
        Me.TBRuta.Name = "TBRuta"
        Me.TBRuta.ReadOnly = True
        Me.TBRuta.Size = New System.Drawing.Size(330, 23)
        Me.TBRuta.TabIndex = 11
        Me.TBRuta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TBRuta.Visible = False
        '
        'TBNombre
        '
        Me.TBNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBNombre.Location = New System.Drawing.Point(189, 253)
        Me.TBNombre.MaxLength = 20
        Me.TBNombre.Name = "TBNombre"
        Me.TBNombre.Size = New System.Drawing.Size(260, 26)
        Me.TBNombre.TabIndex = 13
        Me.TBNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TBNombre.Visible = False
        '
        'LNombre
        '
        Me.LNombre.AutoSize = True
        Me.LNombre.Location = New System.Drawing.Point(99, 255)
        Me.LNombre.Name = "LNombre"
        Me.LNombre.Size = New System.Drawing.Size(72, 20)
        Me.LNombre.TabIndex = 14
        Me.LNombre.Text = "Nombre:"
        Me.LNombre.Visible = False
        '
        'LGetRuta
        '
        Me.LGetRuta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LGetRuta.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LGetRuta.Location = New System.Drawing.Point(509, 191)
        Me.LGetRuta.Name = "LGetRuta"
        Me.LGetRuta.Size = New System.Drawing.Size(26, 23)
        Me.LGetRuta.TabIndex = 16
        Me.LGetRuta.Text = "..."
        Me.LGetRuta.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.LGetRuta.Visible = False
        '
        'OpenFileDialogFile
        '
        Me.OpenFileDialogFile.Filter = "Laberintos |*.lbr"
        Me.OpenFileDialogFile.Title = "Selecciona un archivo de laberinto"
        '
        'RTBLoader
        '
        Me.RTBLoader.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RTBLoader.Location = New System.Drawing.Point(478, 2)
        Me.RTBLoader.MaxLength = 300
        Me.RTBLoader.Name = "RTBLoader"
        Me.RTBLoader.Size = New System.Drawing.Size(83, 12)
        Me.RTBLoader.TabIndex = 20
        Me.RTBLoader.Text = ""
        Me.RTBLoader.Visible = False
        '
        'CBColumnas
        '
        Me.CBColumnas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBColumnas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBColumnas.ForeColor = System.Drawing.Color.DimGray
        Me.CBColumnas.FormattingEnabled = True
        Me.CBColumnas.Items.AddRange(New Object() {"40", "39", "38", "37", "36", "35", "34", "33", "32", "31", "30", "29", "28", "27", "26", "25", "24", "23", "22", "21", "20", "19", "18", "17", "16", "15", "14", "13", "12", "11", "10", "9", "8", "7", "6", "5", "4", "3"})
        Me.CBColumnas.Location = New System.Drawing.Point(229, 205)
        Me.CBColumnas.Name = "CBColumnas"
        Me.CBColumnas.Size = New System.Drawing.Size(60, 28)
        Me.CBColumnas.TabIndex = 23
        Me.CBColumnas.Visible = False
        '
        'CBFilas
        '
        Me.CBFilas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBFilas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBFilas.ForeColor = System.Drawing.Color.DimGray
        Me.CBFilas.FormattingEnabled = True
        Me.CBFilas.Items.AddRange(New Object() {"40", "39", "38", "37", "36", "35", "34", "33", "32", "31", "30", "29", "28", "27", "26", "25", "24", "23", "22", "21", "20", "19", "18", "17", "16", "15", "14", "13", "12", "11", "10", "9", "8", "7", "6", "5", "4", "3"})
        Me.CBFilas.Location = New System.Drawing.Point(228, 169)
        Me.CBFilas.Name = "CBFilas"
        Me.CBFilas.Size = New System.Drawing.Size(60, 28)
        Me.CBFilas.TabIndex = 24
        Me.CBFilas.Visible = False
        '
        'BeforeDesign
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(564, 361)
        Me.Controls.Add(Me.CBFilas)
        Me.Controls.Add(Me.CBColumnas)
        Me.Controls.Add(Me.RTBLoader)
        Me.Controls.Add(Me.LGetRuta)
        Me.Controls.Add(Me.LNombre)
        Me.Controls.Add(Me.TBNombre)
        Me.Controls.Add(Me.TBRuta)
        Me.Controls.Add(Me.LRuta)
        Me.Controls.Add(Me.LIniciar)
        Me.Controls.Add(Me.LColumnas)
        Me.Controls.Add(Me.LFilas)
        Me.Controls.Add(Me.LNuevo)
        Me.Controls.Add(Me.LAbrir)
        Me.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(580, 400)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(580, 400)
        Me.Name = "BeforeDesign"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Maze Studio"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LAbrir As Label
    Friend WithEvents LNuevo As Label
    Friend WithEvents LFilas As Label
    Friend WithEvents LColumnas As Label
    Friend WithEvents LIniciar As Label
    Friend WithEvents LRuta As Label
    Friend WithEvents TBRuta As TextBox
    Friend WithEvents TBNombre As TextBox
    Friend WithEvents LNombre As Label
    Friend WithEvents LGetRuta As Label
    Friend WithEvents OpenFileDialogFile As OpenFileDialog
    Friend WithEvents RTBLoader As RichTextBox
    Friend WithEvents CBColumnas As ComboBox
    Friend WithEvents CBFilas As ComboBox
End Class
